(function($,and){
   /*
   and.templates.load('test-template.html', function(str){
      console.log(str);
   });
   */
   
   tm = new and.ui.Wizzard({
      selector: '#test-div',
      templates: and.templates,
      pages:[
         {
            name: 'test-page',
            template: 'test-page.html',
            type: 'form',
            fields: [
               {
                  name: 'testField',
                  value: 'Hello World!',
                  placeholder: '#test-field',
                  '@readonly':'readonly'
               }
            ]
         },
         {
            name: 'test-page-2',
            template: 'test-page-2.html'
         }
      ],
      flow: ['test-page','test-page-2','test-page-2','test-page']
   });
   $(document).ready(function(){
      tm.showPage('test-page');
      tm.showPage('test-page-2');
      tm.showPage('test-page');
      
      accelerometer.__show();
   });
   
   
   
})(jQuery, and);
