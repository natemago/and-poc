(function($){
   
   var __ID_SEQ = 0;
   
   var __TYPES = {};
   
   var _getType = function(cmp, name){
      if(__TYPES[cmp])
         return __TYPES[cmp][name];
   };
   
   var utils = {
      id: function(pref){
         return (pref || 'gen') + '-' + __ID_SEQ++;
      },
      ext: function(a, b){
         var oa = $.isFunction(a) ? a.prototype : a;
         var ob = $.isFunction(b) ? b.prototype : b;
         $.extend(oa, ob);
         if($.isFunction(a) && $.isFunction(b)){
             a.superclass = ob;
         }
      },
      each: function(iterable, callback, scope){
         scope = scope || window;
         var i = 0;
         for(var k in iterable){
            if(iterable.hasOwnProperty(k)){
               var v = iterable[k];
               if(callback.call(scope, k, v, i) === false){
                  break;
               }
               i++;
            }
         }
      },
      /**
       * Registers new component type under "name"
       */
      register: function(cmp, name, type){
         if(!__TYPES[cmp])
            __TYPES[cmp] = {};
         if(!__TYPES[cmp][name]){
            __TYPES[cmp][name] = type;
         }
      }
   };
   
   
   /**
    * config = {
      "selector": "string|el" - CSS selector string or DOM element,
      "events": {
         "event-name": function(field, [extra arguments]){}
      }
    }
    * This is the base class for all components/objects that can be observed 
    * via events.
    */
   var EventBase = function(config){
      utils.ext(this, config);
      
      this.el = $(config.selector);
      
      var events = {};
      
      /**
       * Registers a listener to the specified event.
       *  name - the name of the event for which we want to register 
       *         the listener for.
       *  callback - the callback function that will be executed when the 
                     specified event is triggered by this component.
                     The callback takes the following arguments:
                        field - the reference to this field.
                        [extra arguments] - any other arguments passed in 
                        "trigger"
       */
      this.on = function(name, callback){
         var registered = events[name];
         if(!registered)
            events[name] = registered = [];

         var self = this;
         var wrapper = function(){
            var args = [self];
            var slice = Array.prototype.slice;
            args.concat(slice.apply(arguments));
            callback.apply(self, args);
         };
         
         $(this.el).bind(name, wrapper);
         registered.push({
            wrapper: wrapper,
            callback: callback
         });
      };
      
      /**
       * Removes a listener callback for the specified event.
       * If the callback argument is given, then only that callback is removed.
       * If the callback argument is ommited - all listeners for the event with
       * "name" are removed.
       */
      this.removeListener = function(name, callback){
         var registered = events[name];
         if(registered){
            if(callback){
               utils.each(registered, function(i, v){
                  if(v.callback == callback){
                     $(this.el).unbind(name, callback);
                     registered.splice(i,1);
                     return false;
                  }
               }, this);
            }else{
               $(this.el).unbind(name);
               delete events[name];
            }
         }
      };
      
      /**
       * Triggers an event with name.
       * The name of the event to be triggered by this component/object is 
       * specified as first argument in the function. You can pass as many
       * arguments as you like to this function, all of them will be passed
       * to the callback registered to this event in the same order as you've
       * passed them in "trigger"
       */
      this.trigger = function(){
         if(arguments.length > 0){
            var event = arguments[0];
            if(events[event]){
               var args = [];
               for(var  i = 1; i < arguments.lenght; i++){
                  args.push(arguments[i]);
               }
               $(this.el).trigger(event, args);
            }
         }
      };
      
      this.init();
   };
   
   
   
   
   utils.ext(EventBase, {
      /**
       * Initializes this component.
       * This is the first method to be called upon instantiation of an object 
       * of this class.
       * This method is excellent extension point for your component, as it is
       * called after the component is fully build and all of the EventBase 
       * functionality is already available. 
       * NOTE: Because this function actually binds the handlers for the events
       * specified in the constructor config object, be careful to call it 
       * if you decide to override this method in your subclass.
       */
      init: function(){
         // initialize the events here...
         var events = this.events || {};
         utils.each(events, function(name, handlers){
            if($.isArray(handlers) || typeof(handlers) == 'object'){
               utils.each(handlers, function(k, handler){
                  this.on(name, handler);
               }, this);
            }else if($.isFunction(handlers)){
               this.on(name, handlers);
            }else{
               // unknown event handler...
            }
         }, this);
      }
   });
   
   
   /**
    * Represents a data field on a form. This usually is one of the following:
    *  - text input field
    *  - checkbox
    *  - text area
    *  - radio button
    *  - button (or 'submit' in terms of HTML markup)
    *  - dropdown (select)
    *  - hidden field
    *  - any custom defined field that will hold/manipulate data on a form
    * 
    * Configuration options (config object specs):
      {
         type: "string", // can be one of: DataField.DEFAULTS.TYPES
         placeholder: "string|DOM Element", // optional, if given, the selector 
                                            // parameter is ignored
         value: "any value", // the value of this field
         "@<any HTML attribute>": "string", // this is raw HTML attribute to be 
                                            // added to the generated markup for
                                            // the specified data field. These
                                            // will be ommited if "selector" is 
                                            // specified in the config object
      }
    */
   var DataField = function(config){
      DataField.superclass.constructor.call(this, config);
   };
   DataField.DEFAULTS = {
      TYPES: {
         'text': {
            'tag': 'input',
            'type': 'text'
          },
         'checkbox': {
            'tag': 'input',
            'type': 'checkbox'
          },
          'radio': {
            'tag': 'input',
            'type': 'radio'
          },
          'hidden': {
            'tag': 'input',
            'type': 'hidden'
          },
          'default': {
            'tag': 'input',
            'type': 'text'
          },
          'select': {
            'tag': 'select'
          },
          'dropdown': {
            'tag': 'select'
          },
          'button': {
            'tag': 'input',
            'type': 'button'
          }
      }
   };
   utils.ext(DataField, EventBase);
   utils.ext(DataField, {
      init: function(){
         if(this.parent && this.parent.selector){
            this._bind(this.parent.selector);
         }
         DataField.superclass.init.call(this);
         if(this.value !== undefined){
            this.setValue(this.value);
         }
      },
      _bind: function(ps){
         var parentEl = $(ps)[0];
         if(this.placeholder){
            // generate the markup
            var elCfg = this._getMarkupConfig();
            var m = this._build(elCfg);
            var el = $(m)[0];
            if(!$(this.placeholder).length){
               this.placeholder = document.body;
            }
            $(this.placeholder).append(el);
            this.selector = el;
         }else{
            this.selector = $(this.selector, parentEl)[0];
         }
      },
      _build: function(cfg){
         var m = ['<',cfg.tag, ' '];
         utils.each(cfg.attributes, function(name,value){
            m.push([name, '="', value, '" '].join(''));
         });
         m.push('>');
         m.push(cfg.body || '');
         m.push(['</',cfg.tag,'>'].join(''));
         return m.join('');
      },
      _getMarkupConfig: function(){
         var d = DataField.DEFAULTS.TYPES[this.type || 'default'] || {
            tag: 'input',
            type: 'text'
         };
         var cfg = {
            tag: d.tag,
            type: d.type,
            attributes: {
               'name': this.name || ''
            }
         };
         utils.each(this, function(name, value){
            if(name[0]=='@'){
               cfg.attributes[name.slice(1)]=value;
            }
         }, this);
         return cfg;
      },
      getValue: function(){
         return $(this.selector, this.context).val();
      },
      setValue: function(value){
         $(this.selector, this.context).val(value);
      },
      validate: function(){
         if(this.validator){
            if($.isFunction(this.validator)){
               return this.validator(this.getValue());
            }else if(typeof (this.validator) == 'string'){
               var r = new RegExp(this.validator);
               var v = this.getValue();
               return typeof(v) == 'string' ? r.test(v) : false;
            }else if(this.validator instanceof RegExp){
               var v = this.getValue();
               return typeof(v) == 'string' ? this.validator.test(v) : false;
            }else{
               return false;
            }
         }else{
            return true; // no validator specified
         }
      }
   });
   
   
    
   
   /**
    * config = {
      template: 'string', // path to the template
      title: 'string', // page title
    }
    */
   var Page = function(config){
      Page.superclass.constructor.call(this, config);
      this.visible = true;
      this.hide();
   };
   
   utils.ext(Page, EventBase);
   utils.ext(Page, {
      init: function(){
         Page.superclass.init.call(this);
      },
      display: function(){
         if(!this.visible){
            $(this.selector).show();
            this.visible = true;
            this.trigger('display');
         }

      },
      hide: function(){
         if(this.visible){
            $(this.selector).hide();
            this.visible = false;
            this.trigger('hide');
         }
         
      },
      close: function(){
         $(this.selector).remove();
         this.destroyed = true;
         this.visible = false;
         this.trigger('destroy');
      }
   });
   
   
   var DataPage = function(config){
      DataPage.superclass.constructor.call(this, config);
   };
   utils.ext(DataPage, Page);
   utils.ext(DataPage, {
      init: function(){
         DataPage.superclass.init.call(this);
         this.on('ready', function(){
            this.bind(this.selector);
         });
      },
      bind: function(el){
         this.data = this.data || {};
         this.fieldsCache = {};
            
         utils.each(this.fields || [], function(i, field){
            this.addField(field);
         },this);
      },
      addField: function(field){
         if(this.fieldsCache[field.name])
            return this.fieldsCache[field.name];
         var _Type = _getType('data-fields', field.type || 'default');
         if(_Type){
            field.parent = this;
            var f = new _Type(field);
            f.name = f.name || utils.id( (f.type || f.id || 'generic-field') );
            this.fieldsCache[f.name] = f;
            return f;
         }
         return undefined;
      },
      getField: function(name){
         return this.fieldsCache[name];
      },
      setData: function(data){
         utils.each(data, function(name, value){
            var f = this.getField(name);
            if(f){
               f.setValue(value);
            }
         }, this);
      },
      getData: function(){
         var retVal = {};
         utils.each(this.fieldsCache, function(name, field){
            retVal[name] = field.getValue();
         }, this);
         return retVal;
      },
      validate: function(){
         var result = {
            valid: true,
            invalidFields: []
         };
         utils.each(this.fieldsCache, function(name, field){
            if(!field.validate()){
               result.valid = false;
               result.invalidFields.push(field);
            }
         }, this);
         return result;
      },
      display: function(data){
         DataPage.superclass.display.call(this);
         if(data !== undefined){
            this.setData(data);
         }
      },
      addField: function(){
         // TODO: 
      }
   });
   
   
   var ViewStack = function(){
      var stack = [];
      
      this.push = function(page){
         var p = undefined;
         var i = 0;
         for (; i < stack.length; i++){
            var e = stack[i];
            if(e.equals){
               if(e.equals(page)){
                  p = e;
                  break
               }
            }else{
               if(e == page){
                  p = e;
                  break;
               }
            }
         };
         if(!p){
            stack.push(page);
            return true;
         }else{
            stack.splice(i,1);
            stack.push(page);
            return false;
         }
      };
      this.pop = function(){
         if(!this.empty())
            return stack.pop();
         return undefined;
      };
      
      this.peek = function(){
         if(stack.length)
            return stack[stack.length-1];
         return undefined;
      };
      
      this.empty = function(){
         return stack.length == 0;
      };
      
      this.popAll = function(criteria){
         var retVal;
         for(var i = stack.length-1; i>=0;i--){
            if( criteria(stack[i]) ){
               break;
            }
         }
         retVal = stack.splice(0,i);
         return retVal;
      };
      
      this.each = function(callback){
         utils.each(stack, function(i,e){
            callback.call(this, e);
         }, this);
      };
      
      this.toString = function(){
         var m = ['View Stack: '];
         if(stack.length){
            for(var i = stack.length-1; i>=0;i--){
               m.push('\t> ' + stack[i]);
            };
         }else{
            m.push('\t(empty)');
         }
         m.push('---------------------');
         return m.join('\n');
      };
   };
   
   var PageManager = function(config){
      PageManager.superclass.constructor.call(this, config);
      this.contentSelector = this.contentSelector || this.selector;
      var pages = this.pages || [];
      this.pages = {};
      utils.each(pages , function(i, page){
         this.pages[page.name] = {
            name: page.name,
            config: page,
            type: page.type,
            toString: function(){
               var m = page.name + ' ';
               if(this.page){
                  m+=this.page.destroyed ? 'destroyed' : 
                     (this.page.visible ? 'visible' : 'hidden');
               }else{
                  m+='- (uninitialized)';
               }
               return m;
            },
            equals: function(a){
               return (a && a.name && a.name == this.name);
            }
        };
      }, this);
      
      this.viewStack = new ViewStack();
      
   };
   utils.ext(PageManager, Page);
   utils.ext(PageManager, {
      init: function(){
         PageManager.superclass.init.call(this);
         // do extra initialization here...
      },
      showPage: function(name, data){
         var pg = this.pages[name];
         var self = this;
         if(pg){
            if(pg.page){
               if(!this.viewStack.empty() && this.viewStack.peek() == pg){
                  return;
               }
               self._showPage(pg, data);
            }else{
               var _Type = _getType('pages', pg.type || 'default');
               if(!_Type)
                  _Type = _getType('pages', 'default');
               if(_Type){
                  var el = $('<div class="page-wrapper"></div>')[0];
                  pg.config.selector = el;
                  pg.page = new _Type(pg.config);
                  $(self.contentSelector).append(el);
                  this.templates.load(pg.config.template, function(tpl){
                     $(el).append(tpl);
                     pg.page.trigger('load');
                     self._showPage(pg, data);
                     pg.page.trigger('ready');
                  }, function(err){
                     alert(err);
                  });
               }
            }
         }
      },
      _hideAll: function(){
         this.viewStack.each(function( pw){
            if(pw.page && pw.page.visible){
               pw.page.hide();
            }
         });
      },
      _showPage: function(pageWrapper, data){
         this._hideAll();
         this.viewStack.push(pageWrapper);
         pageWrapper.page.display(data);
      },
      
      _removeCurrentPageFromView: function(doClose){
         if(!this.viewStack.empty()){
            this._hideAll();
            var curr = this.viewStack.pop();
            
            if(curr && curr.page && doClose) {
               curr.page.close();
               curr.appended = false;
            }
                    
            this.viewStack.popAll(function(pw){
               return !(!pw.page);
            });
            if(!this.viewStack.empty())
               this.viewStack.peek().page.display();
         }
      },
      hidePage: function(){
         this._removeCurrentPageFromView();
      },
      closePage: function(){
         this._removeCurrentPageFromView(true);
      },
      getPage: function(name){
         if(this.pages[name] && this.pages[name].page){
            return this.pages[name].page;
         }
      }
   });
   
   
   /**
    * Wizzard Panel - Page Manager
    * Adds wizzard like functionality to the standard page manager.
    */
   var Wizzard = function(config){
      Wizzard.superclass.constructor.call(this, config);
      var flow = this.flow || [];
      var prev = undefined;
      utils.each(flow, function(i, name){
         var node = {
            prev: prev,
            name: name,
            next: undefined
         };
         if(prev)
            prev.next = node;
         if(!this.current){
            this.current = node;
            this.flow = node;
         }
         prev = node;
      }, this);
   };
   
   utils.ext(Wizzard, PageManager);
   utils.ext(Wizzard, {
      nextPage: function(data){
         if(this.current && this.current.next){
            this.current = this.current.next;
            this.showPage(this.current.name, data);
            this.trigger('next-displayed');
         }else{
            this.trigger('next-stop');
         }
      },
      previousPage: function(){
         if(this.current && this.current.prev){
            this.current = this.current.prev;
            this.showPage(this.current.name);
            this.trigger('prev-displayed');
         }else{
            this.trigger('prev-stop');
         }
      },
      reset: function(){
         this.current = this.flow;
         this.showPage(this.current.name);
      },
      setText: function(button, text){ // button - 'next'|'previous'
         
      },
      popupPage: function(name, data){
         this.showPage(name, data);
      }
   });
   
   
   var TemplateManager = function(config){
      TemplateManager.superclass.constructor.call(this, config);
   };
   
   utils.ext(TemplateManager, EventBase);
   utils.ext(TemplateManager, {
      init: function(){
         this.cache = {};
      },
      loadTemplate: function(name, onLoad, onError){
         var self = this;
         if(this.cache[name]){
            onLoad.call(this, this.cache[name]);
         }else{
            $.ajax({
               url: this.templatesBase + "/" + name, 
               success: function(data){
                  self.cache[name] = data;
                  onLoad.call(self, data);
               },
               error: function(jqXhr, textStatus, error){
                  onError.call(self, textStatus, error);
               }
            });
         }
      }
   });
   

   var __templatesManager = new TemplateManager({
      templatesBase: 'templates',
      selector: {}
   });
   
   utils.ext(window, {
      and:{
         utils: utils,
         ui: {
            EventBase: EventBase,
            Page: Page,
            PageManager: PageManager,
            Wizzard: Wizzard,
            form: {
               DataField: DataField,
               DataPage: DataPage
            }
         },
         templates: {
            setBase: function(base){
               __templatesManager.templatesBase = base;
            },
            load: function(name, success, error){
               __templatesManager.loadTemplate(name, success, error);
            }
         }
      }
   });
   // register basic components
   utils.register('pages', 'default', and.ui.Page);
   utils.register('pages', 'form', and.ui.form.DataPage);
   
   utils.register('data-fields', 'default', and.ui.form.DataField);
   
})(jQuery);
