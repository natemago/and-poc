(function($, and){
   var Adapter = {
      ready: function(callback){
         if(window.cordova || navigator.cordova){
            document.addEventListener("deviceready", function(){
               callback.call(window);
            }, true);
         }else{
            $(document).ready(function(){
               callback.call(window);
            });
         }
      }
   };
   
   
   // export
   and.adapter = Adapter;
   
   
   var __Cordova = {};
   
   // Accelerometer
   var Accelerometer = function(){
      this.watches = {};
      this.acc = {
         x: 0,
         y: 0,
         z: 0
      };
      this.__id_seq = 0;
   };
   
   and.utils.ext(Accelerometer, {
      getCurrentAcceleration: function(success, error){
         this.__callAcc(success, error);
      },
      __callAcc: function(success, error){
         var acc = {
            timestamp: new Date().getTime()
         };
         if(this.simulateError){
            if(error) error();
         }else{
            and.utils.ext(acc, this.acc);
            if(success)success(acc);
         }
      },
      watchAcceleration: function(success, error, options){
         var watchId = "watch-"+this.__id_seq++;
         var watch = {
            success: success,
            error: error
         };
         this.watches[watchId] = watch;
         options = options || {};
         var interval = options.frequency || 10000;
         var self = this;
         watch.intervalId = setInterval(function(){
            var w = self.watches[watchId];
            if(w){
               self.__callAcc(w.success, w.error);
            }
         }, interval);
      },
      clearWatch: function(watchId){
         if(this.watches[watchId])
            delete this.watches[watchId];
      }
   });
   
})(jQuery, and);
